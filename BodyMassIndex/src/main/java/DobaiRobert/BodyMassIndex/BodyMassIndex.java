package DobaiRobert.BodyMassIndex;

import java.util.Scanner;

/**
 * BodyMassIndex.
 *
 * 1.0.0
 * 
 * 2017.11.02.
 * 
 * Készítette Dobai Róbert
 */
public class BodyMassIndex {
	
	/*
	 * A result és a category változók létrehozása, amelyek a számolási eredményt valamint a kategória tárolására szolgálnak.
	 */
	private double result;
	public static String category;
	
	/*
	 * A calculate függyvény megvalósítása, amely kiszámolja a magasságból és tömegből a testtömegindexet (BMI).
	 */
	
	public String calculate(double height, double weight) {
		if (height <= 0.0 || weight <=0.0) { // Null és negatív számok kiszűrése
			/*
			 * Hiba dobása, ha nem megfelelő adatot adunk meg.
			 */
			 throw new IllegalArgumentException("Height or Weight is null or negative! Must be a different value from null and positive!");			   
		} else if ((height > 3)){ // itt ellenőrizzük, hogy méterben vagy centiméterben adta meg a felhasználó a magasságot
		   height = height/100; // a cm méterre való átalakítása
		   result = weight/(height*height); // BMI számolása
		   /*
		    * BMI kiírása az outputra
		    */
		   System.out.print("Your Body Mass Index is: ");
		   System.out.println(result);
		   /*
		    *  Kategória kiírása az outputra
		    */
		   System.out.print("You are: ");
		   if (result < 16) { 
			   category = "Severe Thinness";
			   System.out.println(category);
	   	   } else if (16 < result && result < 17) {
			   category = "Moderate Thinness";
			   System.out.println(category);
		   } else if (17 < result && result < 18.5) {
			   category = "Mild Thinness";
			   System.out.println(category);
		   } else if (18.5 < result && result < 25) {
			   category = "Normal";
			   System.out.println(category);
		   } else if (25 < result && result < 30) {
			   category = "Overweight";
			   System.out.println(category);
		   } else if (30 < result && result < 35) {
	    	   category = "Obese Class I";
	    	   System.out.println(category);
	       } else if (35 < result && result < 40) {
	    	   category = "Obese Class II";
	    	   System.out.println(category);
	       } else {
	    	   category = "Obese Class III";
	    	   System.out.println(category);
	       }
	   } else { // ha méterben van megadva
		   result = weight/(height*height);
		   System.out.print("Your Body Mass Index is: ");
		   System.out.println(result);
		   System.out.print("You are: ");
		   if (result < 16) { 
			   category = "Severe Thinness";
			   System.out.println(category);
	   	   }else if (16 < result && result < 17) {
			   category = "Moderate Thinness";
			   System.out.println(category);
		   } else if (17 < result && result < 18.5) {
			   category = "Mild Thinness";
			   System.out.println(category);
		   } else if (18.5 < result && result < 25) {
			   category = "Normal";
			   System.out.println(category);
		   } else if (25 < result && result < 30) {
			   category = "Overweight";
			   System.out.println(category);
		   } else if (30 < result && result < 35) {
	    	   category = "Obese Class I";
	    	   System.out.println(category);
	       } else if (35 < result && result < 40) {
	    	   category = "Obese Class II";
	    	   System.out.println(category);
	       } else {
	    	   category = "Obese Class III";
	    	   System.out.println(category);
	       }		   
	   }
	   
	return category; // kategória visszaadása a hibakezeléshez
	}
    public static void main( String[] args )
    {
       double height;
	   double weight;
	   
	   Scanner input = new Scanner(System.in);
	   
	   System.out.println("Use comma for decimal format!");
	   System.out.print("Enter your height in centimeters or meters: ");
	   height = input.nextDouble();
	   
	   System.out.print("Enter your weight in kilograms: ");
	   weight = input.nextDouble();
	   
	   input.close();
    	
       BodyMassIndex BMI = new BodyMassIndex();
 	   BMI.calculate(height, weight);  
 	   
    }
}
