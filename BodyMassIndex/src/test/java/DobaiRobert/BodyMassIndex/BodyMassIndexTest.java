package DobaiRobert.BodyMassIndex;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for BodyMassIndex.
 */
public class BodyMassIndexTest {

	private BodyMassIndex bodymassindex;

	@Before
	public void setUp() throws Exception {
		
		bodymassindex = new BodyMassIndex();
	}

	@Test
	public void testCalculateShouldReturnTheNormalValueHeightInMeters() {
		
		String result = bodymassindex.calculate(1.65, 56);
		assertEquals("Normal", result);
	}
	
	@Test
	public void testCalculateShouldReturnTheNormalValueHeightInCentimeters() {
		String result = bodymassindex.calculate(165, 56);
		assertEquals("Normal", result);
	}
	
	@Test
	public void testCalculateShouldReturnTheSevereThinnessValue() {
		
		String result = bodymassindex.calculate(1.65, 40);
		assertEquals("Severe Thinness", result);
	}
	
	@Test
	public void testCalculateShouldReturnTheModerateThinnessValue() {
		
		String result = bodymassindex.calculate(1.65, 45);
		assertEquals("Moderate Thinness", result);
	}
	
	@Test
	public void testCalculateShouldReturnTheMildThinnessValue() {
		
		String result = bodymassindex.calculate(1.65, 48);
		assertEquals("Mild Thinness", result);
	}
	
	@Test
	public void testCalculateShouldReturnTheOverweightValue() {
		
		String result = bodymassindex.calculate(1.65, 70);
		assertEquals("Overweight", result);
	}
	
	@Test
	public void testCalculateShouldReturnTheObeseClassIValue() {
		
		String result = bodymassindex.calculate(1.65, 90);
		assertEquals("Obese Class I", result);
	}
	
	@Test
	public void testCalculateShouldReturnTheObeseClassIIValue() {
		
		String result = bodymassindex.calculate(1.65, 100);
		assertEquals("Obese Class II", result);
	}
	
	@Test
	public void testCalculateShouldReturnTheObeseClassIIIValue() {
		
		String result = bodymassindex.calculate(1.65, 110);
		assertEquals("Obese Class III", result);
	}
	
	@Test
	public void testCalculateShouldThrowExeptionWithTwoNullVariable()	{
		try {
			bodymassindex.calculate(0, 0);
		    fail( "My method didn't throw when I expected it to" );
		} catch (IllegalArgumentException e) {
			System.out.println("Height or Weight is null or negative! Must be a different value from null and positive!");
		}
	}
	
	@Test
	public void testCalculateShouldThrowExeptionWithFirstNullVariable()	{	
		try {
			bodymassindex.calculate(0, 65);
		    fail( "My method didn't throw when I expected it to" );
		} catch (IllegalArgumentException e) {
			System.out.println("Height or Weight is null or negative! Must be a different value from null and positive!");
		}
	}
	
	@Test
	public void testCalculateShouldThrowExeptionWithSecondNullVariable()	{	
		try {
			bodymassindex.calculate(165, 0);
		    fail( "My method didn't throw when I expected it to" );
		} catch (IllegalArgumentException e) {
			System.out.println("Height or Weight is null or negative! Must be a different value from null and positive!");
		}
	}
	
	@Test
	public void testCalculateShouldThrowExeptionWithTwoNegativeVariable()	{
		try {
			bodymassindex.calculate(-165, -56);
		    fail( "My method didn't throw when I expected it to" );
		} catch (IllegalArgumentException e) {
			System.out.println("Height or Weight is null or negative! Must be a different value from null and positive!");
		}				
	}
	
	@Test
	public void testCalculateShouldThrowExeptionWithFirstNegativeVariable()	{
		try {
			bodymassindex.calculate(-165, 56);
		    fail( "My method didn't throw when I expected it to" );
		} catch (IllegalArgumentException e) {
			System.out.println("Height or Weight is null or negative! Must be a different value from null and positive!");
		}				
	}
	
	@Test
	public void testCalculateShouldThrowExeptionWithSecondNegativeVariable()	{
		try {
			bodymassindex.calculate(165, -56);
		    fail( "My method didn't throw when I expected it to" );
		} catch (IllegalArgumentException e) {
			System.out.println("Height or Weight is null or negative! Must be a different value from null and positive!");
		}				
	}
}
